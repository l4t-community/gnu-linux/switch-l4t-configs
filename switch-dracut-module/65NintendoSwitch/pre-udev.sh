#!/bin/bash

# Ready reboot 2 payload
if [ -e "/lib/firmware/reboot_payload.bin" ]; then
	echo 1 > /sys/devices/r2p/default_payload_ready;
fi
