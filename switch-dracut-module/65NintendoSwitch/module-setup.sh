#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh
check() {
	return 0
}

depends() {
	return 0
}

install() {
	inst /lib/firmware/reboot_payload.bin /lib/firmware/reboot_payload.bin
	inst /lib/firmware/tegra21x_xusb_firmware /lib/firmware/tegra21x_xusb_firmware 
	inst /bin/date /bin/date
	inst /bin/xxd /bin/xxd
	inst /bin/dd /bin/dd
	inst /bin/head /bin/head
	inst /bin/grep /bin/grep
	inst /sbin/resize2fs /sbin/resize2fs
	inst /bin/awk /bin/awk
	inst /sbin/mkswap /sbin/mkswap
	inst /sbin/swapon /sbin/swapon
        inst /usr/bin/tar /usr/bin/tar
        inst /usr/bin/gzip /usr/bin/gzip
	inst $moddir/fatal.raw /
	inst_hook pre-udev 65 "$moddir/pre-udev.sh"
	inst_hook pre-pivot 65 "$moddir/pre-pivot.sh"
}
