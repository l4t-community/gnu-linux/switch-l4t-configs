#!/bin/bash

if [ "$1" -eq 0 ]; then
    if grep -q 0 "/sys/devices/70090000.xusb/downgrade_usb3"; then exit 0; else exit 1; fi
elif [ "$1" -eq 1 ]; then
    echo 0 > /sys/devices/70090000.xusb/downgrade_usb3
elif [ "$1" -eq 2 ]; then
    echo 0xffffffff > /sys/devices/70090000.xusb/downgrade_usb3
elif [ "$1" -eq 3 ]; then
     echo "[$2] $3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0" > /sys/devices/thermal-fan-est/coeff
elif [ "$1" -eq 4 ]; then
     if grep -q 0 "/var/lib/nvpmodel/auto_profiles"; then exit 0; else exit 1; fi
elif [ "$1" -eq 5 ]; then
     echo $2 > /var/lib/nvpmodel/auto_profiles
elif [ "$1" -eq 6 ]; then
     exit $(cat /sys/class/power_supply/usb/charge_control_limit)
elif [ "$1" -eq 7 ]; then
     echo $2 > /sys/class/power_supply/usb/charge_control_limit
     echo $2 > /var/lib/nvpmodel/charging_status
elif [ "$1" -eq 8 ]; then
     if [ -e "/sys/class/power_supply/usb/charge_control_limit" ]; then exit 1; else exit 0; fi
elif [ "$1" -eq 9 ]; then
     exit $(cat /var/lib/nvpmodel/charging_status)
fi
    